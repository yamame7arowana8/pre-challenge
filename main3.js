document.getElementById("hello_text").textContent = "はじめてのJavaScript";

let count = 0;

let cells;


// ブロックのパターン
const blocks = {
  i: {           //パターン名
    class: "i",  //クラス名
    pattern: [   //ブロックの配置パターン
      [1, 1, 1, 1]
    ]
  },
  o: {
    class: "o",
    pattern: [
      [1, 1], 
      [1, 1]
    ]
  },
  t: {
    class: "t",
    pattern: [
      [0, 1, 0], 
      [1, 1, 1]
    ]
  },
  s: {
    class: "s",
    pattern: [
      [0, 1, 1], 
      [1, 1, 0]
    ]
  },
  z: {
    class: "z",
    pattern: [
      [1, 1, 0], 
      [0, 1, 1]
    ]
  },
  j: {
    class: "j",
    pattern: [
      [1, 0, 0], 
      [1, 1, 1]
    ]
  },
  l: {
    class: "l",
    pattern: [
      [0, 0, 1], 
      [1, 1, 1]
    ]
  }
};


loadTable();  //表の作成

//1秒間に行う処理
setInterval(function () {
  count++;
  document.getElementById("hello_text").textContent = "はじめてのJavaScript(" + count + ")";
  if(hasFallingBlock()){  //落下中のブロックがあるとき
    fallBlocks();         //ブロックを落とす
  }else{  　　　　　　　　 //落下中のブロックがないとき
    generateBlock();　　　//ブロックの生成
  }
}, 1000);

/* ------ ここから下は関数の宣言部分 ------ */

//表の作成
function loadTable() {
  cells = [];  //配列の宣言
  let td_array = document.getElementsByTagName("td");　//表のすべてのマスを取得
  let index = 0;
  for (let row = 0; row < 20; row++) {　　 //行の0から
    cells[row] = [];                       //2次元配列にする
    for (let col = 0; col < 10; col++) {
      cells[row][col] = td_array[index];　 //表のマス1つ1つに行列の番号を当てはめていく
      index++;
    }
  }

}

//ブロックを落とす
function fallBlocks() {
  // 一番下の行のクラスを空にする
  //for (var i = 0; i < 10; i++) {
    //if(cells[19][i].className !== ""){  //一番下の行にブロックがあるならば落ちているブロックがない
      //isFalling = false;  //落下中のブロックがない
      //return;　　　　　　　//処理の停止、一番下のブロックを落とさない
    //}
  //}
  //底についていないか
  for(let col = 0; col < 10; col++){
    if(cells[19][col].blockNum === fallingBlockNum){  //一番下の行と落ちているブロックの番号が同じ
      isFalling = false;　　//落ちているブロックはない
      return;　//処理の停止
    }
  }
  //1つ下のマスに別のブロックがないか
  for(let row = 18; row>= 0; row--){
    for(let col = 0;col < 10; col++){
      if(cells[row][col].blockNum === fallingBlockNum){
        //1つ下にあるブロックと落ちているブロックが違うならば
        if(cells[row + 1][col].className !== "" && cells[row + 1][col].blockNum !== fallingBlockNum){
          isFalling = false;
          return;  //処理の停止
        }
      }
    }
  }
  // 下から二番目の行から繰り返しクラスを下げていく
  for (let row = 18; row >= 0; row--) {
    for (let col = 0; col < 10; col++) {
      if (cells[row][col].blockNum === fallingBlockNum) {　//もしクラス名があればそれを一つ下の行に移す
        cells[row + 1][col].className = cells[row][col].className;
        cells[row + 1][col].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";  //移動させたあとはクラス名を消す
        cells[row][col].blockNum = null;
      }
    }
  }
}

//落下中のブロックの確認
let isFalling = false;  //落下中のブロックがない状態から始まる
function hasFallingBlock() {
  // 落下中のブロックがあるか確認する
  return isFalling;　　//ブロックがあるときtrueを返す
}

let fallingBlockNum = 0;
//ランダムにブロックを生成
function generateBlock() {
  // ランダムにブロックを生成する
  // 1. ブロックパターンからランダムに一つパターンを選ぶ
  let keys = Object.keys(blocks);  //i,o,t,s,z,j,lを取得(配列)
  let nextBlockKey = keys[Math.floor(Math.random() * keys.length)];  //i~l[数字]
  let nextBlock = blocks[nextBlockKey];
  let nextFallingBlockNum=fallingBlockNum + 1;  
  // 2. 選んだパターンをもとにブロックを配置する
  const pattern = nextBlock.pattern;　//ランダムに選ばれたブロックのパターンを取得
  for (let row = 0; row < pattern.length; row++) {
    for (let col = 0; col < pattern[row].length; col++) {
      if (pattern[row][col]) {
        cells[row][col + 3].className = nextBlock.class;  //左から4番目からパターンを配置
        cells[row][col + 3].blockNum = nextFallingBlockNum;   //番号を振る
      }
    }
  }
  // 3. 落下中のブロックがあるとする
  isFalling = true;  //落下中のブロックがある
  fallingBlockNum = nextFallingBlockNum;
}
